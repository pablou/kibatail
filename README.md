# kibatail

kibatail is a command line utility to query and tail ELK (elasticsearch, logstash, kibana) logs through Kibana. In case you have direct access to elasticsearch, you should use elktail: https://github.com/knes1/elktail. Otherwise, if you're trapped behind IT-security lines and just want to perform the old and good tail -f, but you only have access to kibana, this is the right choice.

usage: kibatail.py [-h] -u URL [-s SECONDS] [--match MATCH]
                   [--unmatch UNMATCH] [--refresh REFRESH]
                   [--environment ENVIRONMENT] [--verbose] [--query QUERY]

optional arguments:
  -h, --help            show this help message and exit
  -u URL, --url URL     kibana base server url. Do not include
                        /elasticsearch/.... i.e.:
                        http://kibana.sharedbt.service
  -s SECONDS, --seconds SECONDS
                        logs are not usually 'live' in ELK, they get buffered
                        and sent over from logstash to elasticsearch every X
                        seconds. So we need to move our query X seconds back
                        in time in order to receive logs. This parameter
                        represents the delay in seconds between a log being
                        written locally in the server till its availability in
                        elasticsearch, in order to get a live feed. If the
                        delay is 10 seconds, then this parameter should be set
                        to 12 or 15. DEFAULT=30
  --match MATCH         comma-separated service names to include in the
                        search. i.e: "service1,service2,etc"
  --unmatch UNMATCH     comma-separated service names to exclude from the
                        search. i.e: "service1,service2,etc"
  --refresh REFRESH, -r REFRESH
                        refresh period in seconds. DEFAULT=5
  --environment ENVIRONMENT
                        environment. DEFAULT='LIVE'
  --verbose, -v         verbose mode
  --query QUERY, -q QUERY
                        text to query kibana for
						
example: 
]$ python kibatail.py -u http://kibana.domain.com/ --match my_service_name --environment live --refresh 5 --seconds 60 --query MyQuery