import requests
import argparse
from datetime import datetime
import time
import logging

headers = {
    'Accept-Encoding': 'gzip, deflate',
    'kbn-version': '5.4.1',
    'content-type': 'application/x-ndjson',
    'Accept': 'application/json'
}

secondsBack = 30
refreshPeriod = 5
match=[]
unmatch=[]
environment="live"

parser = argparse.ArgumentParser()

parser.add_argument("-u", "--url", help="kibana base server url. Do not include /elasticsearch/.... i.e.: http://kibana.domain.com", required=True)
parser.add_argument("-s", "--seconds", help="logs are not usually 'live' in ELK, they get buffered and sent over from logstash to elasticsearch every X seconds. So we need to move our query X seconds back in time in order to receive logs. This parameter represents the delay in seconds between a log being written locally in the server till its availability in elasticsearch, in order to get a live feed. If the delay is 10 seconds, then this parameter should be set to 12 or 15. DEFAULT=30", type=int)
parser.add_argument("--match", help="comma-separated service names to include in the search. i.e: \"service1,service2,etc\"")
parser.add_argument("--unmatch", help="comma-separated service names to exclude from the search. i.e: \"service1,service2,etc\"")
parser.add_argument("--refresh", "-r", help="refresh period in seconds. DEFAULT=5")
parser.add_argument("--environment", help="environment. DEFAULT='LIVE'")
parser.add_argument("--verbose", "-v", help="verbose mode", action="store_true")
parser.add_argument("--query", "-q", help="text to query kibana for")

args = parser.parse_args()


if args.seconds:
    secondsBack = int(args.seconds)

if args.refresh:
    refreshPeriod = int(args.refresh)

if args.match:
    for svc in args.match.split(","):
        match.append({"match":{"service_name":{"query":svc,"type":"phrase"}}})

if args.unmatch:
    for svc in args.unmatch.split(","):
        unmatch.append({"match":{"service_name":{"query":svc,"type":"phrase"}}})

if args.environment:
    environment = args.environment

if args.verbose:
    logging.basicConfig(level=logging.DEBUG)

if args.query:
    match.append({"query_string": {"query": args.query, "analyze_wildcard": 'true'}})

url = args.url
if not(url.endswith('/')):
    url=url+'/'
url += "elasticsearch/_msearch"

#ADD Environment
match.append({"match": {"env":{"query": environment,"type": "phrase"}}})

lastCheck = (int(datetime.now().strftime("%s")) - secondsBack - refreshPeriod)*1000

while True:
    must = (','.join(map(str,match))).replace("'", "\"")
    must_not = (','.join(map(str,unmatch))).replace("'", "\"")

    #ADD Time range to query
    thisCheck = (int(datetime.now().strftime("%s")) - secondsBack)*1000

    timeRange = {"range":{"@timestamp":{"format":"epoch_millis","gte":lastCheck,"lte":thisCheck}}}
    must += "," + str(timeRange).replace("'", "\"")

    data = '{}\n{"query":{"bool":{"must":['+ must +'],"must_not":[' + must_not + ']}},"size":500,"sort":[{"@timestamp":{"order":"asc","unmapped_type":"boolean"}}]}\n'

    logging.debug("QUERYING KIBANA FOR LOGS....")

    res = requests.post(url=url,
                        data=data,
                        headers=headers)

    if res.status_code != 200:
        logging.warning("KIBATAIL => KIBANA RETURNED AN ERROR, POSSIBLY DUE TO ANTI-FLOODING PROTECTION. WILL DOUBLE THE REFRESH PERIOD FROM " + refreshPeriod + " to " + refreshPeriod*2 + " IN ORDER TO SPACE THE QUERIES")
        refreshPeriod*=2
    else:
        response = res.json()

        if len(response['responses']) > 0 and len(response['responses'][0]['hits']['hits'])>0:
            for log in response['responses'][0]['hits']['hits']:
                print(log['_source']['source'] + " - " + log['_source']['message'])

        lastCheck=thisCheck
    
    time.sleep(refreshPeriod)